// maximum grid size
#define MAXSZ 10

int load_grid(FILE* infile);

bool find_word_up(char grid[][MAXSZ], char w[], int i, int j);
bool find_word_down(char grid[][MAXSZ], int n, char w[], int i, int j);
bool find_word_left(char grid[][MAXSZ], char w[], int i, int j);
bool find_word_down(char grid[][MAXSZ], int n, char w[], int i, int j);

void write_all_matches(char grid[][MAXSZ], int n, char w[]);
