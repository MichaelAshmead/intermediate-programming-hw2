#define _GNU_SOURCE

#include <stdlib.h>
#include <string.h>
#include <stdio.h>
#include <stdbool.h>
#include <ctype.h>
#include "wordsearch.h"

// NOTE: this file should not contain any functions other than main();
// other functions should go in wordsearch.c, with their prototypes in
// wordsearch.h

int main(int argc, char* argv[]) {
   char lettergrid[MAXSZ][MAXSZ];
   //initialize lettergrid to '0's
   for (int x = 0; x < MAXSZ; x++) {
      for (int y = 0; y < MAXSZ; y++) {
         lettergrid[x][y] = '0';
      }
   }

   FILE* f;
   f = fopen(argv[1], "r");
   if (argc == 1) {
      printf("Please enter a file name!\n");
      return 1;
   }

   int readResult = load_grid(f);
   if (readResult == 0)
      printf("%s\n", "The grid is invalid.");
   else { //put input into char array
      char* line = NULL;
      size_t len = 0;
      ssize_t read;
      
      int rowcounter = 0;
      rewind(f);
      while ((read = getline(&line, &len, f)) != -1) {
         for (int x = 0; x < readResult; x++)
            lettergrid[rowcounter][x] = tolower(line[x]);
         rowcounter++;
      }

      write_all_matches(lettergrid, readResult, "pw");
   }

   fclose(f);
   return 0;
}
