#define _GNU_SOURCE

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdbool.h>
#include "wordsearch.h"

// NOTE: all the functions declared in wordsearch.h should be
// implemented in this file.

int load_grid(FILE* infile) {
   char* line = NULL;
   size_t len = 0;
   ssize_t read;

   int charcounter = 0;
   int allcharcounter;
   int rowcounter = 0;
   int pos = 0;
   while((read = getline(&line, &len, infile)) != -1) {
      while (line[pos] != '\0' && line[pos] != '\n') {
        charcounter++;
        pos++;
      }
      if (rowcounter == 0)
         allcharcounter = charcounter;
      else if (allcharcounter != charcounter)
         return 0;
      charcounter = 0;
      pos = 0;
      rowcounter++;
   }
   if (rowcounter != allcharcounter)
      return 0;
   else if (rowcounter == 0 || rowcounter > 10 || allcharcounter == 0 || allcharcounter > 10)
      return 0;

   return rowcounter;
}

bool find_word_up(char grid[][MAXSZ], char w[], int i, int j) {
   int pos = 0;
   bool result = false;
   while (grid[i][j] == w[pos] && i >= 0) {
      pos++;
      i--;
      if (w[pos] == '\0') {
          result = true;
          break;
      }
   }
   return result;
}

bool find_word_down(char grid[][MAXSZ], int n, char w[], int i, int j) {
   int pos = 0;
   bool result = false;
   while (grid[i][j] == w[pos] && i <= n) {
      pos++;
      i++;
      if (w[pos] == '\0') {
          result = true;
          break;
      }
   }
   return result;
}

bool find_word_left(char grid[][MAXSZ], char w[], int i, int j) {
   int pos = 0;
   bool result = false;
   while (grid[i][j] == w[pos] && j >= 0) {
      pos++;
      j--;
      if (w[pos] == '\0') {
          result = true;
          break;
      }
   }
   return result;
}

bool find_word_right(char grid[][MAXSZ], int n, char w[], int i, int j) {
   int pos = 0;
   bool result = false;
   while (grid[i][j] == w[pos] && j <= n) {
      pos++;
      j++;
      if (w[pos] == '\0') {
          result = true;
          break;
      }
   }
   return result;
}

void write_all_matches(char grid[][MAXSZ], int n, char w[]) {
   for (int x = 0; x < n; x++) {
      for (int y = 0; y < n; y++) {
         bool up = find_word_up(grid, w, x, y);
         bool down = find_word_down(grid, n, w, x, y);
         bool left = find_word_left(grid, w, x, y);
         bool right = find_word_right(grid, n, w, x, y);
         if (up)
            printf("%s %i %i %c\n", w, x, y, 'U');
         else if (down)
            printf("%s %i %i %c\n", w, x, y, 'D');
         else if (left)
            printf("%s %i %i %c\n", w, x, y, 'L');
         else if (right)
            printf("%s %i %i %c\n", w, x, y, 'R');
      }
   }

}
